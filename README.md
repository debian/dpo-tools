# README

version 1.6

dpo-tools is a set of shell scripts that can aid in the translation of Debian's
podebconf files.

dpo-tools does not dictate any rule of how translations should be done, it just
tries to reflect the conventions used in the mailing list
debian-l10n-portuguese@lists.debian.org list.

The scripts are as follows:

* dpo-get-templates: downloads a templates.pot file from the podebconf
templates repository.

* dpo-init-po: creates and initializes a podebconf file from a file templates.

* dpo-apply-templates: updates a podebconf file with a new version of your
templates file.

* dpo-apply-compendium: updates a odebconf file with strings already translated,
from a compendium file.

* dpo-check: verifies if the podebconf file is well formed.

* dpo-diff: shows information in a diff format colorful and paged.

* dpo-make-patch: creates a patch file based on two versions of a podebconf file.

* dpo-wrap: breaks the rows of a podebconf file in column 80.

## INSTALLATION

Copy the scripts to some directory in your PATH.

	$ cp scripts/dpo-* ~/bin/
	or
	$ sudo cp sripts/dpo-* /usr/local/bin/

## DEPENDENCIES

dpo-tools depends on other programs to work correctly. You must have the
following major programs installed on your computer:

* gettext utilities: msgfmt, msgcat, msgmerge
* awk
* colordiff

To install them on a Debian system or derivative:

	$ sudo apt install gettext gawk colordiff

You will also want to use the podebconf-display-po program. To install it on a
Debian system or derivative:

	$ sudo apt install po-debconf

## CONFIGURATION

* dpo-init-po.awk: adjust the NOME and EMAIL variables. If you use gawk, the
variable ANO must be adjusted at the beginning of each year. You can uncomment
the line that automatically detects the year, if your awk interpreter has
support for the strftime() function.

* dpo-init.po: edit the last line of the file to reflect the full path of the
dpo-init.po.awk file, for instance: /etc/dpo-init-po.awk

* dpo-make-patch: set the variable ID with the initials of your name.

* dpo-apply-compendium: adjust the COMPENDIUM variable to reflect the full path
of the compendium file, downloaded from http://i18n.debian.org/compendia/pt_BR/

## UNINSTALLATION

Just delete the files.

## EXAMPLES

To initiate a new translation: the following commands sequence downloads a
podebconf templates file, creates a podebconf file based on the downloaded
templates file, initializes the podebconf file with frequently used information,
updates the podebconf file with compendium strings, edits the podebconf file,
breaks the lines of the podebconffile in column 80, checks if the podebconf file
is well formed ands tests the display of the podebconf file:

	$ dpo-get-templates package
	$ dpo-init-po package_version_templates.pot
	$ dpo-apply-compendium package_pt_BR.po
	$ vi package_pt_BR.po
	$ dpo-wrap package_pt_BR.po
	$ dpo-check package_pt_BR.po
	$ podebconf-display-po package_pt_BR.po
	send the package_pt_BR.po file to RFR

To review a translation made by another translator and generate a patch with
change suggestions:

    replace "me" with your initials in the following commands:
    $ vi /etc/dpo-tools/make-patch
	$ cp package_pt_BR.po package_pt_BR.me.po
	$ vi package_pt_BR.me.po
	$ dpo-wrap package_pt_BR.me.po
	$ dpo-check package_pt_BR.me.po
	$ podebconf-display-po package_pt_BR.me.po
	$ dpo-make-patch package_pt_BR.po
    send the generated patch file, responding the RFR

To apply a new version of the templates file to a podebconf file previously
translated:

	$ dpo-get-templates package_pt_BR.po
	$ dpo-apply-templates package_pt_BR.po

## CONTRIBUTING

Contributions are welcome either by creating issues in git repository or in the
form of merge requests. Please access:

https://salsa.debian.org/l10n-br-team/dpo-tools

## AUTHORS

* Adriano Rafael Gomes <adrianorg@arg.eti.br>.
* Paulo Henrique de Lima Santana <phls@debian.org>.
* Debian Brazilian Localization Team <contato@debianbrasil.org.br>

## LEIAME

A LEIAME file is available in:

htps://salsa.debian.org/l10n-br-team/dpo-tools-doc

## COPYRIGHT

	Copyright (C) 2008,2012 Adriano Rafael Gomes <adrianorg@arg.eti.br>.
	Copyright (C) 2018      Debian Brazilian Localization Team <contato@debianbrasil.org.br>.
	Copyright (C) 2023,2025 Paulo Henrique de Lima Santana <phls@debian.org>.
	Licença GPLv3+: GNU GPL versão 3 ou posterior
	<http://gnu.org/licenses/gpl.html>.
	This is free software: you are free to change and redistribute it.
	There is NO WARRANTY, to the extent permitted by law.

Original source:

https://www.arg.eti.br/blog/pages/software/dpo-tools/manual-do-usuario-do-dpo-tools.html
